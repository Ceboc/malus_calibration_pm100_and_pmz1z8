include(joinpath("src","data_adquisition.jl"))
include(joinpath("src","data_analysis_and_plot.jl"))

# 1 PASOS POR GRADO, 1500 MEDICIONES POR PASO
θs, potencias =  toma_mediciones(361,10_000)

plot_y_ajt(θs,potencias)

println("FIN DE PROGRAMA")