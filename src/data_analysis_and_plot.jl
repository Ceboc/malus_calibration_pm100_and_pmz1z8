using LsqFit
using Plots, LaTeXStrings

function plot_y_ajt(θs,potencias)
    model(x,p) =@. p[1]*cosd(x-p[2])^2 + p[3]
    p= [abs(-(extrema(potencias)...)), 0.0, minimum(potencias)]
    
    fit = curve_fit(model, θs, potencias,p)
    
    adj = map(x->model(x,fit.param),θs)
    
    @show fit.param 
    
    begin
        p = scatter(θs,potencias, label = "Medido")
        plot!((x -> x.val).(θs),(x-> x.val).(adj), label = "Ajuste\nA=$(fit.param[1])\nx₀=$(fit.param[2])")
        xlabel!(L"\theta\;[^\circ]")
        ylabel!("Power")
        plot!(legend=:outerright)
        
        savefig(p, joinpath("figs","theta_vs_power.png"))
        savefig(p, joinpath("figs","theta_vs_power.pdf"))
        display(p)
    end

    return fit.param
end
