using PyCall
using ProgressMeter
using Statistics
using Measurements
using CSV, DataFrames

# Carga de paquetes para VISA
visa = try
    pyimport("pyvisa")
catch
    run(`$(PyCall.python) -m pip install pyvisa`)
    run(`$(PyCall.python) -m pip install pyvisa-py`)
    run(`$(PyCall.python) -m pip install serial`)
    run(`$(PyCall.python) -m pip install pyserial`)
    run(`$(PyCall.python) -m pip install pyusb`)
    pyimport("pyvisa")
end

# inicializacion POWER METER
rm = visa.ResourceManager()

pm_dir = rm.list_resources()

pm = rm.open_resource(pm_dir[1])

@show pm.query("*IDN?")
pm.baudrate = 115200
pm.write("CONF:POW")

function recupera_potencia()
    pm.query_ascii_values("READ?")[1]
end

@time recupera_potencia()


# Leer documentación de pylablib para ver qué dependencias
# se necesitan para que corra este código
Thorlabs = try
    pyimport("pylablib.devices.Thorlabs")
catch
    run(`$(PyCall.python) -m pip install pylablib"[devio-full]"`)
    pyimport("pylablib.devices.Thorlabs")
end

@show l_t = Thorlabs.list_kinesis_devices()

KDC = Thorlabs.KinesisMotor(l_t[1][1], scale="PRM1-Z8")


# Generación de arreglos y toma de mediciones

function toma_mediciones(no_θs, no_potencias)

    θs = round.(collect(range(0, 360, no_θs)),digits = 4)

    potencias = zeros(no_θs)

    potencias = measurement.(potencias)

    j = 1
    @showprogress for θ in θs

        KDC.move_to(θ)
        sleep(0.1)

        #status = KDC.get_status()[]
        println("\nMoviendo a $(θ)°, paso $j de $(no_θs)")
        while KDC.get_status()[end-1] == "active"
            sleep(0.1)
        end

        parcial = zeros(no_potencias)
        println("Midiendo potencias")
        @showprogress for i in eachindex(parcial)
            parcial[i] += recupera_potencia()
        end
        potencias[j] = measurement(mean(parcial), std(parcial))

        println("Última potencia: $(potencias[j]) W")

        j += 1
    end

    # Guardado de datos
    CSV.write(joinpath("data", "theta_vs_power.csv"), DataFrame((theta=θs, potencia=potencias)))

    # Se devuelven los valores con sus respectivas incertidumbres
    # PRM1Z8 inc. mitad de la mínima escala
    measurement.(θs,(5/60)/2), potencias
end